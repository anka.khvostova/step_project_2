const { src, dest, watch } = require('gulp');
const gulp = require('gulp');
const file_include = require('gulp-file-include'); //собирает разбитый по частям html файл
const browser_sync = require('browser-sync').create();//работа с браузером
const del = require('del');//чистит папку
const scss = require('gulp-sass'); //компилирует файлы scss в css
const autoprefixer = require('gulp-autoprefixer');// добавляет автопрефиксы браузеров
const group_media = require('gulp-group-css-media-queries'); // собирает воедино медиа-запросы
const clean_css = require('gulp-clean-css'); // чистит и минимизирует(сжимает) код сss
const rename = require('gulp-rename'); //переименивывает файл сss ==> .min.css
const imagemin = require('gulp-imagemin');
const javascript = require('gulp-concat');



const project_folder = './dist';
const source_folder = './src';
const path = {
    build: {
        html: project_folder + "/",
        css: project_folder + '/css/',
        js: project_folder + "/js/",
        img: project_folder + "/img/",
        fonts: project_folder + "/fonts/",
    },
    src: {
        html: [source_folder + "/*.html", "!" + source_folder + "/_*.html"],
        // html: "*.html",
        scss: source_folder + '/scss/main.scss',
        js: source_folder + "/js/app.js",
        img: source_folder + "/img/**/*.{jpg,png,svg}",
        fonts: source_folder + "/fonts/*.ttf",
    },
    watch: {
        html: source_folder + "/**/*.html",
        scss: source_folder + '/scss/**/*.scss',
        js: source_folder + "/js/**/*.js",
        img: source_folder + "/img/**/*.{jpg,png,svg}",

    },
    clean: "./" + project_folder + "/"

}

function browserSync() {
    browser_sync.init({
        server: {
            // baseDir: path.src.html,
            baseDir: "./dist/",
        },
        port: 5500,
        notify: false,
        browser: "firefox",
    });

}

function jsCreate() {
    return src(path.src.js)
        .pipe(javascript('app.js'))
        .pipe(dest(path.build.js))
        .pipe(browser_sync.stream())
}


function htmlCreate() {
    console.log(path.src.html);
    return src(path.src.html)
        .pipe(file_include())
        .pipe(dest(path.build.html))
        .pipe(browser_sync.stream())

}
function cssCreate() {
    return src(path.src.scss)
        // .pipe(file_include())
        .pipe(scss({ outputStyle: "expanded" }).on("error", scss.logError))
        // .pipe(scss({
        //     outputStyle: "expanded"
        // }))
        .pipe(group_media())
        .pipe(autoprefixer({
            overrideBrowserslist: ["last 5 versions"],
            cascade: true,
        }))
        .pipe(dest(path.build.css))//первая выгрузка до минимизации
        .pipe(clean_css())
        .pipe(rename({
            extname: ".min.css"
        }))

        .pipe(dest(path.build.css))//вторая выгрузка, после минимизации
        .pipe(browser_sync.stream())
}

function imageCreate() {
    return src(path.src.img)
        .pipe(imagemin())
        .pipe(dest(path.build.img))
        .pipe(browser_sync.stream())

}

function watchFiles() {
    // gulp.watch([path.watch.html], htmlCreate);
    // gulp.watch([path.watch.scss], cssCreate);
    // gulp.watch([path.watch.img], imageCreate);

    watch(path.watch.html).on("change", function () {
        htmlCreate();
        browser_sync.reload();
    });
    watch(path.watch.scss).on("change", function () {
        cssCreate();
        // htmlCreate();
        browser_sync.reload();
    });
    watch(path.watch.img).on("change", function () {
        imgCreate();
        browser_sync.reload();
    });
    watch(path.watch.js).on("change", function () {
        jsCreate();
        browser_sync.reload();
    });

    // gulp.watch(path.watch.html, function (cb) {
    //     htmlCreate();
    //     browser_sync.reload();
    //     cb();
    // });
    // gulp.watch(path.watch.scss, function (cb) {
    //     cssCreate()
    //     browser_sync.reload();
    //     cb();
    // });
}

function cleanDist() {
    return del(path.clean)
}


const build = gulp.series(cleanDist, gulp.parallel(htmlCreate, cssCreate, imageCreate, jsCreate));
const watch1 = gulp.parallel(build, watchFiles, browserSync);
// const watch1 = gulp.series(build, watchFiles, browserSync);
// const watch = gulp.parallel(build, watchFiles);
exports.scss = cssCreate;
exports.html = htmlCreate;
exports.build = build;
// exports.watch = watch;
exports.default = watch1;
exports.browser = browserSync;
exports.files = watchFiles;
exports.img = imageCreate;
exports.js = jsCreate;